import io
import sys
import collections

import numpy as np
import pandas as pd

__all__ = [
    "Metrics", 
    "BinaryMetrics",
    "MulticlassMetrics",
    "metrics"
]

class Metrics(object):
    def __init__(self, y, y_hat):
        self.y = y
        self.y_hat = y_hat.loc[y.index]
        self.ix_y = ~np.isnan(self.y)
        self.ix_y_hat = ~np.isnan(self.y_hat)
        self.eq = self.y == self.y_hat

    @property
    def N(self):
        return self.y.shape[0]

    @property
    def NNZ(self):
        return self.ix_y_hat.sum()

    def __repr__(self):
        o = io.StringIO()
        for key, fn_name in self._attributes:
            value = getattr(self, fn_name)
            print(key, value, sep="\t", file=o)
        return o.getvalue()
     
class BinaryMetrics(Metrics):
    _attributes = [
        ("N", "N"),
        ("NNZ", "NNZ"),
        ("TP", "TP"),
        ("FP", "FP"),
        ("FN", "FN"),
        ("Precision", "precision"),
        ("Recall", "recall"),
        ("Lift", "lift")
    ]

    def __init__(self, y, y_hat):
        assert y.dtype == np.bool
        assert y_hat.dtype == np.bool
        super(BinaryMetrics,self).__init__(y, y_hat)

    @property
    def TP(self):
        return (self.y_hat & self.eq).sum()

    @property
    def FP(self):
        return (self.y_hat & (~self.eq)).sum()
    
    @property
    def FN(self):
        return ((~self.y_hat) & self.y).sum()

    @property
    def precision(self):
        return self.TP / (self.TP + self.FP)

    @property
    def recall(self):
        return self.TP / (self.TP + self.FN)

class RegressionMetrics(Metrics):
    _attributes = [
        ("N", "N"),
        ("NNZ", "NNZ"),
        ("MAD", "MAD"),
        ("MSE", "MSE"),
    ]

    def __init__(self, y, y_hat):
        super(RegressionMetrics,self).__init__(y, y_hat)

    @property
    def MAD(self):
        y = self.y.dropna()
        #y_hat = self.y_hat.fillna(self.y_hat.mean()).loc[y.index]
        y_hat = self.y_hat.loc[y.index]
        return (y - y_hat).apply(np.abs).mean()

    @property
    def MSE(self):
        y = self.y.dropna()
        #y_hat = self.y_hat.fillna(self.y_hat.mean()).loc[y.index]
        y_hat = self.y_hat.loc[y.index]
        return (y - y_hat).apply(lambda x: x ** 2).mean()

class MulticlassMetrics(Metrics):
    _attributes = [
            ("N", "N"),
            ("NNZ", "NNZ"),
            ("Classes", "n_classes"),
            ("Macro Precision", "macro_precision"),
            ("Macro Precision MLE", "macro_precision_mle"),
            ("Macro Recall", "macro_recall"),
            ("Micro Precision", "micro_precision"),
            ("Micro Recall", "micro_recall"),
    ]

    def __init__(self, y, y_hat, min_class_size=0):
        if (min_class_size is not None) and (min_class_size > 0):
            y_hat = y_hat.loc[y.index]
            c = y.value_counts()
            ok = y.isin(c[c >= min_class_size].index)
            y = y.ix[ok]
            y_hat = y_hat.ix[ok]
            
        super(MulticlassMetrics, self).__init__(y, y_hat)
        self._sub = {}
        for label in set(self.y):
            if not np.isnan(label):
                y_ = self.y == label
                y_hat_ = self.y_hat == label
                self._sub[label] = BinaryMetrics(y_, y_hat_)

    @property
    def n_classes(self):
        return len(self._sub)

    @property
    def macro_precision(self):
        tp = sum(s.TP for s in self._sub.values())
        fp = sum(s.FP for s in self._sub.values())
        return tp / (tp + fp)

    @property
    def macro_recall(self):
        tp = sum(s.TP for s in self._sub.values())
        fn = sum(s.FN for s in self._sub.values())
        return tp / (tp + fn)

    @property
    def macro_precision_mle(self):
        c = self.y.dropna().value_counts().iloc[0]
        n = self.y.dropna().shape[0]
        return c / n

    def _micro_average(self, prop):
        n = sum = 0
        for s in self._sub.values():
            x = getattr(s, prop)
            if not np.isnan(x):
                n += 1
                sum += x
        return sum / n

    @property
    def micro_precision(self):
        return self._micro_average("precision")

    @property
    def micro_recall(self):
        return self._micro_average("recall")

    @property
    def micro_lift(self):
        return self._micro_average("lift")

    def report(self):
        print("", "N", "Precision", "Recall", sep="\t")
        size = lambda k: self._sub[k].y.sum()
        for label in sorted(self._sub, key=lambda k: -size(k)):
            m = self._sub[label]
            print(label, size(label), m.precision, 
                    m.recall, sep="\t")

def metrics(y, y_hat, type=None, min_class_size=0):
    unique = set(y.dropna())
    assert len(unique) > 1

    if type is not None:
        if type == ("binary", "b"):
            return BinaryMetrics(y, y_hat)
        elif type in ("multiclass", "mc"):
            return MulticlassMetrics(y, y_hat, 
                    min_class_size=min_class_size)
        elif type in ("continuous", "c"):
            assert y.dtype != object
            return RegressionMetrics(y, y_hat)

    if y.dtype == np.float64:
        return RegressionMetrics(y, y_hat)
    else:
        return MulticlassMetrics(y, y_hat, 
                min_class_size=min_class_size)
