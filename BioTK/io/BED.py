import numpy as np

from BioTK.genome import Region, region

class BEDFile(object):
    def __init__(self, path_or_handle):
        if isinstance(path_or_handle, str):
            self._handle = open(path_or_handle, "r")
        else:
            self._handle = path_or_handle

    def _parse(self, line):
        fields = line.rstrip("\n").split("\t")
        contig = fields[0]
        start = int(fields[1])
        end = int(fields[2])
        name = ""
        score = np.nan
        strand = "."
        try:
            name = fields[3]
            try:
                score = float(fields[4])
            except ValueError:
                pass
            strand = fields[5] if fields[5] in ("-","+") else "."
        except IndexError:
            pass
        return region(contig, start, end, name, score, strand)

    def __iter__(self):
        for i,line in enumerate(self._handle):
            try:
                yield self._parse(line)
            except:
                raise Exception("Bad BED format at line %s: %s" % (i, line))

    def to_array(self):
        return np.array(list(self))

def test():
    import io
    buffer = io.StringIO(
"""chr1	1	2	name	0.0	-
chr2	5	10	.	.	malformed
chr3	5	10""")
    h = BEDFile(buffer)
    print(h.to_array())

if __name__ == "__main__":
    test()
