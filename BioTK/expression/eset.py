__all__ = ["ExpressionSet"]

import pandas as pd

def to_matrix(path_or_matrix):
    if isinstance(path_or_matrix, str):
        return pd.read_csv(path_or_matrix,
                sep="\t", index_col=0)
    elif isinstance(path_or_matrix, pd.DataFrame):
        return path_or_matrix
    else:
        raise ValueError

def to_series(path_or_categorical):
    if isinstance(path_or_matrix, str):
        return pd.read_csv(path_or_matrix, sep="\t",
                header=None,
                index_col=0).iloc[:,0]

def seteq(x,y):
    return len(set(x) & set(y)) == len(x) == len(y)

class ExpressionSet(object):
    def __init__(self, expression, design, groups,
            taxon_id=None):

        self._design = to_matrix(design)
        self._expression = to_matrix(expression)

        assert seteq(self._design.index, 
                self._expression.columns)

        # Metadata
        self._metadata = {}
        if isinstance(taxon_id, int):
            self._metadata["taxon_id"] = taxon_id

    #####################
    # Convenience getters
    #####################

    @property
    def taxon_id(self):
        return self._metadata.get("taxon_id")

    #####
    # I/O
    #####

    def save(path):
        pass

    @staticmethod
    def load(path):
        pass

    ##########
    # Analysis
    ##########

    def differential_expression(self, formula):
        from BioTK.r import edger
        # TODO: allow contrast vector instead of formula

        #assert seteq(contrast.index, self._design.index)
        #design, contrast = self._design.align(contrast)
        #print(design, contrast)
        o = edger(self._expression, self._design, 
                formula=formula, coefficients=["Age"])
        return o
