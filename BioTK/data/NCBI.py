"""
Convenience functions to download and parse commonly used Entrez
datasets.
"""

import gzip
import io
import os
import tarfile
from collections import defaultdict
import sqlite3

import numpy as np
import pandas as pd
import pyfasta
from Bio import Entrez

from BioTK import LOG
from BioTK.io.NCBI import CACHE_ROOT
from BioTK.io import download
from BioTK.genome import Region, region

Entrez.email = "mail@corygil.es"

TABLES = {
    "taxon": """
    CREATE TABLE taxon (
        id INTEGER PRIMARY KEY NOT NULL,
        name VARCHAR,
        common_name VARCHAR
    );""",

    "gene": """
    CREATE TABLE gene (
        id INTEGER PRIMARY KEY NOT NULL,
        taxon_id INTEGER,
        symbol VARCHAR,
        name VARCHAR,

        FOREIGN KEY (taxon_id) REFERENCES taxon (id)
    );""",

    "gene_locus": """
    CREATE TABLE gene_locus (
        gene_id INTEGER NOT NULL,

        contig VARCHAR,
        start INTEGER,
        end INTEGER,
        strand CHAR,

        FOREIGN KEY (gene_id) REFERENCES gene (id)
    );""",

    "homology": """
    CREATE TABLE homology (
        group_id INTEGER NOT NULL,
        gene_id INTEGER NOT NULL
    );
    """
}

INDEX = """
CREATE INDEX IF NOT EXISTS ix_gene_taxon_id ON gene(taxon_id);
CREATE INDEX IF NOT EXISTS ix_gene_locus_gene_id 
    ON gene_locus(gene_id);
CREATE INDEX IF NOT EXISTS ix_homology_group_id 
    ON homology(group_id);
CREATE INDEX IF NOT EXISTS ix_homology_gene_id
    ON homology(gene_id);
CREATE INDEX IF NOT EXISTS ix_gene_symbol
    ON gene(symbol);
CREATE INDEX IF NOT EXISTS ix_gene_symbol_ci
    ON gene(symbol COLLATE NOCASE);
"""

DB_PATH = os.path.expanduser("~/.local/share/BioTK/ncbi.db")

def load_taxon(db):
    def records():
        scientific = {}
        common = {}
        url = "ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz"
        path = download(url, unzip="gzip", open=False)
        tar = tarfile.open(path)
        handle = tar.extractfile("names.dmp")
        for i,line in enumerate(handle):
            fields = [c.strip("\t") for c in 
                    line.decode("utf-8")\
                            .rstrip("\n").split("|")][:-1]
            taxon_id = int(fields[0])
            name = fields[1]
            unique_name = fields[2]
            name_class = fields[3]
            if name_class == "scientific name":
                scientific[taxon_id] = name
            elif "common name" in name_class:
                common[taxon_id] = name
        for taxon_id in scientific:
            yield taxon_id, scientific[taxon_id], \
                common.get(taxon_id)
    c = db.cursor()
    c.executemany("INSERT INTO taxon VALUES (?,?,?)", 
            records())

def load_gene(db):
    def records():
        c = db.cursor()
        c.execute("SELECT id FROM taxon")
        taxa = list(r[0] for r in c)

        url = "ftp://ftp.ncbi.nih.gov/gene/DATA/gene_info.gz"
        path = download(url, open=False)
        df = pd.read_table(path,
                compression="gzip",
                skiprows=1,
                names=["Taxon ID", "Gene ID", "Symbol", "Name"],
                usecols=[0,1,2,11])
        df.drop_duplicates("Gene ID", inplace=True)
        ix = df["Taxon ID"].isin(taxa)
        for taxon_id, gene_id, symbol, name in df.ix[ix,:]\
                .to_records(index=False):
            symbol = None if symbol == "-" else symbol
            name = None if name == "-" else name
            yield int(gene_id), int(taxon_id), symbol, name
    c = db.cursor()
    c.executemany("INSERT INTO gene VALUES (?,?,?,?)", records())

def load_gene_locus(db):
    c = db.cursor()
    c.execute("SELECT id FROM gene")
    genes = set(r[0] for r in c)

    def records():
        url = "ftp://ftp.ncbi.nih.gov/gene/DATA/gene2refseq.gz"
        path = download(url, open=False)
        with gzip.open(path, "rt") as h:
            next(h)
            i = 0
            for line in h:
                fields = line.rstrip("\n").split("\t")
                try:
                    gene_id = int(fields[1])
                    if gene_id not in genes:
                        continue
                    contig = fields[7]
                    start = int(fields[9])
                    end = int(fields[10])
                    strand = fields[11]
                    if not strand in ("+", "-"):
                        strand = None
                except ValueError:
                    continue
                yield gene_id, contig, start, end, strand

    c = db.cursor()
    c.executemany("INSERT INTO gene_locus VALUES (?,?,?,?,?)",
            records())

def load_homology(db):
    url = "ftp://ftp.ncbi.nih.gov/pub/HomoloGene/" + \
            "current/homologene.data"
    def records():
        with download(url) as h:
            pairs = set()
            for line in h:
                fields = line.rstrip("\n").split("\t")
                group_id = int(fields[0])
                gene_id = int(fields[1])
                pair = (group_id, gene_id)
                if pair not in pairs:
                    yield group_id, gene_id
                pairs.add(pair)
    c = db.cursor()
    c.executemany("INSERT INTO homology VALUES (?,?)",
            records())

def index(db):
    db.executescript(INDEX)

def load(db):
    c = db.cursor()
    c.execute("SELECT name FROM sqlite_master WHERE type='table';")
    existing_tables = set(r[0] for r in c)

    tables = ["taxon", "gene", "gene_locus", "homology"]
    for tbl in tables:
        if not tbl in existing_tables:
            db.executescript(TABLES[tbl])
        fn = globals()["load_%s" % tbl]
        try:
            c = db.cursor()
            c.execute("SELECT * FROM %s LIMIT 1" % tbl)
            next(c)
        except StopIteration:
            LOG.info("Loading NCBI data table: %s" % tbl)
            fn(db)
            db.commit()

def connect(local=False):
    create = not os.path.exists(DB_PATH)
    os.makedirs(os.path.dirname(DB_PATH), exist_ok=True)
    db = sqlite3.connect(DB_PATH)
    # FIXME do only if create
    load(db)
    index(db)
    return db

def query(sql):
    db = connect()
    c = db.cursor()
    c.execute(sql)
    return iter(c)

#######
# Query
#######

def _taxon_homologs(taxon_id, method="homologene"):
    assert method == "symbol"
    db = connect()
    return pd.read_sql_query("""
    SELECT g1.id as "Gene ID", 
        g2.taxon_id as "Homolog Taxon ID", 
        g2.id as "Homolog Gene ID"
    FROM gene g1
    INNER JOIN gene g2
    ON g1.symbol=g2.symbol
    WHERE g1.taxon_id = %s
    AND g2.taxon_id != %s
    COLLATE NOCASE;
    """ % (taxon_id, taxon_id), db)

def _gene_homologs(gene_id, method="homologene"):
    if method == "homologene":
        db = connect()
        c = db.cursor()
        try:
            group_id = next(query("""
                SELECT group_id 
                FROM homology
                WHERE gene_id=%s""" % gene_id))[0]
        except StopIteration:
            return {}
        o = dict(query("""
            SELECT gene.taxon_id, homology.gene_id
            FROM homology
            INNER JOIN gene
            ON gene.id=homology.gene_id
            WHERE homology.group_id=%s""" % group_id))
        del o[gene_id]
        return o
    elif method == "symbol":
        db = connect()
        symbol = Gene.info(gene_id)["Symbol"]
        return pd.read_sql_query("""
            SELECT taxon_id as 'Taxon ID', id as 'Gene ID'
            FROM gene
            WHERE symbol='%s' COLLATE NOCASE
            """ % symbol.lower(),
            db)
    else:
        raise ValueError("Invalid method: %s" % method)

class Taxonomy(object):
    @staticmethod
    def homologs(taxon_id):
        assert isinstance(taxon_id, int)
        return _taxon_homologs(taxon_id, method="symbol")

    @staticmethod
    def genes(taxon_id):
        assert isinstance(taxon_id, int)
        rows = query("""SELECT id, symbol, name 
            FROM gene
            WHERE taxon_id=%s""" % taxon_id)
        return pd.DataFrame.from_records(rows, 
                columns=["Gene ID", "Symbol", "Name"],
                index="Gene ID")

    def loci(taxon_id):
        assert isinstance(taxon_id, int)
        rows = query("""SELECT contig, start, end, 
            gene_id, NULL, strand
            FROM gene_locus
            INNER JOIN gene
            ON gene.id=gene_locus.gene_id
            WHERE gene.taxon_id=%s""" % taxon_id)
        return np.array(list(rows), dtype=Region)

class Gene(object):
    @staticmethod
    def homologs(gene_id, method="homologene"):
        return _gene_homologs(gene_id, method=method)

    @staticmethod
    def info(gene_id):
        return pd.Series(next(query("""SELECT id, symbol, name
            FROM gene
            WHERE id=%s""" % gene_id)),
            index=["Gene ID", "Symbol", "Name"])

    @staticmethod
    def locus(gene_id):
        rows = query("""SELECT contig, start, end, 
                gene_id, NULL, strand
            FROM gene_locus
            WHERE gene_id=%s""" % gene_id)
        return region(*next(rows))

    @staticmethod
    def promoter_locus(gene_id, upstream=500, downstream=0):
        locus = Gene.locus(gene_id)
        contig, start, end, name, strand = map(locus.__getitem__,
                ["contig", "start", "end", "name", "strand"])
        if strand == "-":
            nstart = end - downstream
            nend = end + upstream
        else:
            nstart = start - upstream
            nend = start + downstream
        nstart = max(nstart, 0)
        #FIXME: nend should be bounded by contig length
        return region(contig, nstart, nend, name, np.nan, strand)

class Refseq(object):
    CACHE = {}

    @staticmethod
    def sequence(locus):
        q = {"chr": locus["contig"], "strand": locus["strand"],
             "start": locus["start"], "stop": locus["end"]}
        fa = Refseq.nucleotide(locus["contig"])
        return fa.sequence(q)

    @staticmethod
    def nucleotide(accession):
        # Two levels of caching are used here:
        # 1. The FASTA files are cached in a directory,
        # 2. pyfasta handles are cached in memory after first
        #   use in the CACHE variable

        cachedir = os.path.join(CACHE_ROOT, "refseq")
        if not os.path.exists(cachedir):
            os.makedirs(cachedir)
        path = os.path.join(cachedir, accession)

        if not os.path.exists(path):
            LOG.info("Cache miss for Refseq accession: %s" 
                    % accession)
            query = Entrez.esearch(db="nucleotide", term=accession)
            record = Entrez.read(query)
            fa = Entrez.efetch(db="nucleotide", 
                    id=record["IdList"][0], 
                    rettype="fasta")
            fa.readline()
            o = open(path, "wt")
            print(">", accession, sep="", file=o)
            for line in fa:
                line = line.rstrip("\n")
                if line:
                    print(line, file=o)
            o.close()

        if not accession in Refseq.CACHE:
            Refseq.CACHE[accession] = pyfasta.Fasta(path)
        return Refseq.CACHE[accession]

if __name__ == "__main__":
    #locus = Gene.promoter_locus(1000)
    #contig = Refseq.nucleotide("NC_000018.10")
    #s = Refseq.sequence(locus)
    import sys
    #Taxonomy.homologs(9606).to_csv(sys.stdout, sep="\t", 
    #        index=False, header=True)
    gene_id = int(sys.argv[1])
    Gene.homologs(gene_id, method="symbol").to_csv(sys.stdout, sep="\t",
            index=False, header=True)
